# HTML, JSON, dan XML
Pertama, HTML (Hyper Text Markup Language) merupakan kumpulan text yang didalamnya dalam dibuat menjadi sebuah link yang dapat berpindah dari satu halaman ke lainnya sehingga disebut sebagai hypertext. HTML merupakan sebuah markup language karena adanya tanda unduk tiap-tiap text yang ada yang disebut dengan HTML tag.

Kedua, JSON (JavaScript Object Notation) merupakan format data berbasis bahasa meta yang digunakan untuk menyimpan informasi dengan cara yang terorganisir dan mudah diakses dengan ekstensi file .json. JSON seringkali digunakan untuk mengirim data dari server ke halaman web. JSON menurut beberapa orang sangatlah mudah dipahami karena basisnya yang sangat deskriptif sehingga kumpulan data yang tersedia dapat dengan mudah dipahami oleh manusia secara logis. Terakhir, JSON juga language-independent yaitu dapat digunakan dengan programming language apapun.

Ketiga, XML (eXtensible Markup Language) adalah bahasa markup hampir layaknya HTML dengan ekstensi file .xml. XML didesain untuk menyimpan data dan mengangkut data. Selayaknya JSON, XML juga sangat deskriptif. Dengan menggunakan XML kita dapat menyimpan data dengan format dan struktur. Namun demikian, XML bukanlah sebuah bahasa pemrograman karena tidak ada pembentukan algoritma maupun komputasi sehingga tidak terdapat aturan penulisan dalam pembuatannya yang turut menjadikannya sebagai kelebihan. Namun, terdapat case sensitive yang ada pada huruf besar atau kecil pada XML yang memungkinkan kalian untuk menentukan elemen markup sehingga dapat menciptakan bahasa markup yang sesuai dengan keinginan masing-masing.


# Apakah perbedaan antara JSON dan XML?
Terdapat beberapa perbedaan antara JSON dan XML, perbedaan paling mendasar dari JSON dan XML adalah JSON berorientasi pada data sedangkan XML berorientasi pada dokumen. Selanjutnya, terdapat keuntungan dalam menggunakan JSON yaitu ukuran dari file yang dimiliki lebih kecil dibandingkan dengan XML. Hal ini lah yang menjadikan JSON unggul dalam kecepatan pengiriman data jika dibandingkan dengan XML. 

Secara umum, JSON lebih sederhana dibanding XML. Hal ini dikarenakan kode yang perlu dibuat degan menggunakan JSON cenderung sangat sederhana sehingga mudah dibaca untuk program pada umumnya. Sedangkan XML, kode yang perlu dibuat cenderung ketinggalan zaman karena kompleks dan juga panjang. Namun, dengan adanya kompleksitas ini, XML terbilang lebih kuat dibandingkan JSON. Dalam pembuatan aplikasi dengan kebutuhan kompleks seputar data interchange, fitur-fitur XML dapat menurunkan resiko perangkat lunak secara signifikan. Namun, dalam segi keamanan JSON dinilai jauh lebih baik dibandingkan XML yang cukup rentan terhadap serangan perluasan entitas eksternal dan validasi DTD.

Pada sisi bahasa, XML merupakan bahasa markup layaknya HTML sedangkan JSON merupakan format yang ditulis dalam JavaScript. Dari penulisan bahasa dan kode inilah kita juga dapat melihat adanya perbedaan dalam penyimpanan data yang dilakukan oleh keduanya, seperti XML menggunakan tree structure sedangkan JSON menggunakan pasangan key-value.

# Apakah perbedaan antara HTML dan XML?
Terdapat beberapa perbedaan antara HTML dan XML, perbedaan paling mendasar adalah XML merupakan sebuah framework untuk menentukan bahasa markup, sedangkan HTML adalah sebuah bahasa markup. Secara struktural, XML memiliki informasi yang telah disediakan sedangkan HTML tidak. Meski sekilas XML dan HTML mirip, namun mereka memiliki tujuan yang berbeda. HTML menitikberatkan tujuannya dalam penyajian data atau pemformatan sedangkan XML lebih menitikberatkan pada transfer data yang berpacu pada struktur dan konteksnya. Walaupun begitu, XML dan HTML saling melengkapi satu sama lain.

Dalam proses penulisan terdapat beberapa perbedaan antara HTML dan XML, seperti pada penulisa huruf kecil dan besar sangatlah diperhatikan pada XML tidak seperti HTML. Selanjutnya, dalam HTML dan XML keduanya terdapat tag karena berhubungan dengan markup language, tetapi pada XML penggunaan tag bersifat wajib tidak seperti HTML dimana terdapat beberapa jenis elemen yang tidak memerlukan tag. Selain itu, tag pada XML tidak ditentukan sebelumnya sedangkan HTML sudah terdapat tag yang telah ditentukan sebelumnya. Terakhir, proses nesting atau pengurutan pada XML sangatlah diperhatikan tidak seperti HTML yang tidak terlalu memperhatikan hal tersebut. 


# Referensi
APA Perbedaan JSON Dan XML? (2020, December 21). Monitor Teknologi. https://www.monitorteknologi.com/perbedaan-json-dan-xml/
Perbedaan XML Dan HTML: Fitur Dan Kuncinya. (2020, October 9). DosenIT.com. https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html
Difference between JSON and XML. (2019, February 19). GeeksforGeeks. https://www.geeksforgeeks.org/difference-between-json-and-xml/
JSON vs XML: Which one is better? (n.d.). Blog | Imaginary Cloud. https://www.imaginarycloud.com/blog/json-vs-xml/
What is JSON. (n.d.). W3Schools Online Web Tutorials. https://www.w3schools.com/whatis/whatis_json.asp
Safris, S. (2019, 18). A deep look at JSON vs. XML, Part 1: The history of each. Toptal Engineering Blog. https://www.toptal.com/web/json-vs-xml-part-1
JSON example. (n.d.). JSON. https://json.org/example.html
Joshi, V. (n.d.). Why you should be using JSON vs XML. Cloud Elements Blog | API Integration Platform. https://blog.cloud-elements.com/using-json-over-xml
How to use Serializers in the Django Python web framework. (n.d.). Opensource.com. https://opensource.com/article/20/11/django-rest-framework-serializers
JSON XMLHttpRequest. (n.d.). W3Schools Online Web Tutorials. https://www.w3schools.com/js/js_json_http.asp
Serializing Django objects. (n.d.). Django documentation | Django documentation | Django. https://docs.djangoproject.com/en/3.2/topics/serialization/
Convert HTML document to JSON - Tool Slick. (2017, September 27). https://toolslick.com/conversion/data/html-to-json