from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm
from django.http.response import HttpResponseRedirect

def index(request):
    Notes = Note.objects.all()
    response = {'Note': Notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
  
    form = NoteForm(request.POST or None)
      
    if (request.method == 'POST' and form.is_valid) :
        # save the form data to model
        form.save()
        return HttpResponseRedirect('/lab-4')
  
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    Notes = Note.objects.all()
    response = {'Note': Notes}
    return render(request, "lab4_note_list.html", response)
