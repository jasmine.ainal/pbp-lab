import 'package:flutter/material.dart';

// This is my screen
class FormScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FormScreenState();
  }
}

class FormScreenState extends State<FormScreen> {
  String _name;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildName() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'Masukan nomor pemeriksaan...'),
      keyboardType: TextInputType.number,
      onSaved: (String value) {
        _name = value;
        showDialog<void>(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Hasil Pemeriksaan Tes Covid-19'),
              content: const Text('POSITIF' ),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('OK'),
                ),
              ],
            );
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Slow Lab")),
      body: SingleChildScrollView(
        child: Container(
          child: Container(
            margin: EdgeInsets.all(24),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 100),
                  Container(
                  margin: EdgeInsets.only(top: 25, bottom: 5),
                  child: Text("Cek Hasil Pemeriksaan Tes Covid-19",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w700)),
                ),
                  _buildName(),
                  SizedBox(height: 50),
                  RaisedButton(
                    child: Text(
                      'SUBMIT',
                      style: TextStyle(color: Colors.blue.shade900, fontSize: 16),
                    ),
                    onPressed: () {
                      if (!_formKey.currentState.validate()) {
                        return;
                      }

                      _formKey.currentState.save();
                      print("Sedang melakukan pengecekan hasil dari nomor pemeriksaan: " + _name);
                      //Send to API
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
