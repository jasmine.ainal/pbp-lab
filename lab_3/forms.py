from lab_1.models import Friend
from django import forms

class FriendForm(forms.ModelForm):
    # specify the name of model to use
    class Meta:
        model = Friend
        fields = "__all__"