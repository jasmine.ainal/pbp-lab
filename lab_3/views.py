from django.shortcuts import render
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all() # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    context ={}
  
    # create object of form
    form = FriendForm(request.POST or None)
      
    # check if form data is valid
    if (request.method == 'POST' and form.is_valid) :
        # save the form data to model
        form.save()
        return HttpResponseRedirect('/lab-3')
  
    context['form']= form
    return render(request, "lab3_form.html", context)