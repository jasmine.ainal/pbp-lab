from django.http import response
from django.http.response import HttpResponse
from django.core import serializers
from django.shortcuts import render
from .models import Note

# Create your views here.

def index(request):
    Notes = Note.objects.all() # TODO Implement this
    response = {'Note': Notes}
    return render(request, 'lab2.html', response)

def xml(request):
    Notes = Note.objects.all() # TODO Implement this
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    Notes = Note.objects.all() # TODO Implement this
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")