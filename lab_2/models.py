from django.db import models

# Create your models here.

class Note(models.Model):
    to_placeholder = models.CharField(max_length=30)
    from_placeholder = models.CharField(max_length=30)
    title_placeholder = models.CharField(max_length=30)
    message_placeholder = models.CharField(max_length=30)